package com.example.fcm;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.biometric.BiometricManager;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.example.fcm.zxing.android.CaptureActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.spongycastle.asn1.nist.NISTNamedCurves;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

import static com.example.fcm.R.id.txt;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
	private static final String AUTH_KEY = "AAAAjGipi60:APA91bExp6Dw6I33EZ-noe8UgeL3I06m2dLyINsIS6C835DSfzS6R8dJroE0cL31JEyDeHncYsHI-tRkWykB0Ji7aZimiRfhxmj_J9jb8cGBQiiyCgiKDACfc750Ffzrz_tMlEzP6ife";
	private TextView mTextView;
	private String token;
	private static final String DECODED_CONTENT_KEY = "codedContent";
	private static final String DECODED_BITMAP_KEY = "codedBitmap";
	private static final int REQUEST_CODE_SCAN = 0x0000;

	private Button btn_scan;
	private TextView tv_scanResult;
	private static final String TAG = "MainActivity";

	public static final String BROADCAST_ACTION = "com.example.fcm.ECPOINT";
	private Connection connectionInstance;

	private Handler handler = new Handler();

	private Executor executor = new Executor() {
		@Override
		public void execute(Runnable command) {
			handler.post(command);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTextView = findViewById(txt);

		tv_scanResult = (TextView) findViewById(R.id.txt);
		btn_scan = (Button) findViewById(R.id.btn_scan);
		btn_scan.setOnClickListener(this);

		ProtocolClass.curve = NISTNamedCurves.getByName("P-256").getCurve();

		// 安装一次得到k，不能进程结束每次打开都新的k。
		// Reference https://cloud.tencent.com/developer/ask/153453/answer/265786
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String kstr = prefs.getString("k_str", "null");
		if ("null".equals(kstr)) {
			ProtocolClass.k = ProtocolClass.genK();
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("k_str", ProtocolClass.k.toString());
			editor.apply();
		} else {
			ProtocolClass.k = new BigInteger(kstr);
		}
		Log.d(TAG, "Get k:" + ProtocolClass.k.toString(16));

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			String tmp = "";
			for (String key : bundle.keySet()) {
				Object value = bundle.get(key);
				tmp += key + ": " + value + "\n\n";
			}
			mTextView.setText(tmp);
		}

		FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
			@Override
			public void onComplete(@NonNull Task<InstanceIdResult> task) {
				if (!task.isSuccessful()) {
					token = task.getException().getMessage();
					Log.w("FCM TOKEN Failed", task.getException());
				} else {
					token = task.getResult().getToken();
					ProtocolClass.deviceToken = token;
					Log.i("FCM TOKEN", token);
				}
			}
		});

		BroadcastReceiver br = new BroadcastReceiver() {
			@SuppressLint("Assert")
            @Override
			public void onReceive(Context context, Intent intent) {
				HashMap<String, String> msg = (HashMap<String, String>) intent.getSerializableExtra("ecpoint");
				int model = intent.getIntExtra("model", -1);
				assert msg != null;
				assert model != -1;
				Log.d(TAG, "Message Data MSG: "+msg);
				connectionInstance = new Connection();
				connectionInstance.calAlpha(msg);

				// 无交互
				if (model == 0) {
				    confirmYES();
                }
				// 对话框确认
				if (model == 1) {
                    // https://stackoverflow.com/questions/20405070/how-to-use-dialog-fragment-showdialog-deprecated-android
                    DialogFragment newFragment = ConfirmDialogFragment.newInstance("请确认登录请求！");
                    newFragment.show(getSupportFragmentManager(), "dialog");
                }
				// 指纹验证确认
				if (model == 2) {
					// https://developer.android.com/training/sign-in/biometric-auth#java
					checkBioSupport();
					showBiometricPrompt();
                }
			}
		};
		// https://developer.android.com/guide/components/broadcasts
		IntentFilter filter = new IntentFilter();
		filter.addAction(BROADCAST_ACTION);
		this.registerReceiver(br, filter);
	}

	public void showToken(View view) {
		mTextView.setText(token);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_scan:
				//动态权限申请
				if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
				} else {
					goScan();
				}
				break;
			default:
				break;
		}
	}

	/**
	 * 跳转到扫码界面扫码
	 */
	private void goScan(){
		Intent intent = new Intent(MainActivity.this, CaptureActivity.class);
		startActivityForResult(intent, REQUEST_CODE_SCAN);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		switch (requestCode) {
			case 1:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					goScan();
				} else {
					Toast.makeText(this, "你拒绝了权限申请，可能无法打开相机扫码哟！", Toast.LENGTH_SHORT).show();
				}
				break;
			default:
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 扫描二维码/条码回传
		if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
			if (data != null) {
				//返回的文本内容
				String content = data.getStringExtra(DECODED_CONTENT_KEY);
				//返回的BitMap图像
				Bitmap bitmap = data.getParcelableExtra(DECODED_BITMAP_KEY);

				tv_scanResult.setText("你扫描到的内容是：" + content);

				ProtocolClass.clientToken = content;

				new Thread(new Runnable() {
					@Override
					public void run() {
						JSONObject jTokenPayload = new JSONObject();
						try {
							assert ProtocolClass.deviceToken != null;
							jTokenPayload.put("token", ProtocolClass.deviceToken);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						Log.d("MSGTOCLIENT", jTokenPayload.toString());
						ProtocolClass.sendMsgToClient("token", jTokenPayload);
					}
				}).start();
			}
		}
	}

	public void confirmYES() {
		connectionInstance.computeBeta(ProtocolClass.k);
		final Map<String, String> resultbeta = connectionInstance.getBeta();
//				ProtocolClass.beta = ProtocolClass.computeBeta(ProtocolClass.alpha, ProtocolClass.k).normalize();
		new Thread(new Runnable() {
			@Override
			public void run() {
				JSONObject jECPointPayload = new JSONObject();
				try {
					jECPointPayload.put("ok", "true");
					jECPointPayload.put("x", resultbeta.get("x"));
					jECPointPayload.put("y", resultbeta.get("y"));
					jECPointPayload.put("description", "beta");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				Log.d("MSGTOCLIENT", "jECPointPayload"+jECPointPayload.toString());
				ProtocolClass.sendMsgToClient("ecpoint", jECPointPayload);
			}
		}).start();
	}

	public void confirmNO() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				JSONObject jECPointPayload = new JSONObject();
				try {
					jECPointPayload.put("ok", "false");
					jECPointPayload.put("description", "Device refused your login!");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				Log.d("MSGTOCLIENT", "jECPointPayload"+jECPointPayload.toString());
				ProtocolClass.sendMsgToClient("ecpoint", jECPointPayload);
			}
		}).start();
	}

	private void checkBioSupport() {
		// TODO: 增加提示框
		BiometricManager biometricManager = BiometricManager.from(this);
		switch (biometricManager.canAuthenticate()) {
			case BiometricManager.BIOMETRIC_SUCCESS:
				Log.d(TAG, "App can authenticate using biometrics.");
				break;
			case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
				Log.e(TAG, "No biometric features available on this device.");
				break;
			case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
				Log.e(TAG, "Biometric features are currently unavailable.");
				break;
			case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
				Log.e(TAG, "The user hasn't associated any biometric credentials " +
						"with their account.");
				break;
		}
	}

	private void showBiometricPrompt() {
		BiometricPrompt.PromptInfo promptInfo =
				new BiometricPrompt.PromptInfo.Builder()
						.setTitle("Biometric Auth for PM")
						.setSubtitle("Log in using your biometric credential")
						.setNegativeButtonText("Cancel")
						.build();

		BiometricPrompt biometricPrompt = new BiometricPrompt(MainActivity.this,
				executor, new BiometricPrompt.AuthenticationCallback() {
			@Override
			public void onAuthenticationError(int errorCode,
											  @NonNull CharSequence errString) {
				super.onAuthenticationError(errorCode, errString);
				Toast.makeText(getApplicationContext(),
						"Authentication error: " + errString, Toast.LENGTH_SHORT)
						.show();
				confirmNO();
			}

			@Override
			public void onAuthenticationSucceeded(
					@NonNull BiometricPrompt.AuthenticationResult result) {
				super.onAuthenticationSucceeded(result);
				BiometricPrompt.CryptoObject authenticatedCryptoObject =
						result.getCryptoObject();
				confirmYES();
				// User has verified the signature, cipher, or message
				// authentication code (MAC) associated with the crypto object,
				// so you can use it in your app's crypto-driven workflows.
			}

			// TODO: 验证失败手机有5次验证机会，应该5次都失败才confirmNO。现在是每失败一次就confirmNO一次。
			@Override
			public void onAuthenticationFailed() {
				super.onAuthenticationFailed();
				Toast.makeText(getApplicationContext(), "Authentication failed",
						Toast.LENGTH_SHORT)
						.show();
				confirmNO();
			}
		});

		// Displays the "log in" prompt.
		biometricPrompt.authenticate(promptInfo);
	}


}