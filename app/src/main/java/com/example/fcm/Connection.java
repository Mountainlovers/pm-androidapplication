package com.example.fcm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.json.JSONException;
import org.json.JSONObject;
import org.spongycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Connection {
    private ECPoint alpha = null;
    private ECPoint beta = null;
    private boolean flag = false;

    public void computeBeta(BigInteger k) {
        assert alpha != null;
        assert k != null;
        this.beta = this.alpha.multiply(k).normalize();
        Log.d("DEBUG", alpha.toString());
        Log.d("DEBUG", beta.toString());
    }

    public Map<String, String> getBeta() {
        Map<String, String> result = new HashMap<String, String>();
        result.put("x", beta.getAffineXCoord().toString());
        result.put("y", beta.getAffineYCoord().toString());
        return result;
    }

    public void calAlpha(HashMap<String, String> msg) {
        Log.d("Connection", "Message Data MSG: "+msg);
        String x = msg.get("x");
        String y = msg.get("y");
        this.alpha = ProtocolClass.retrivePoint(x, y);
    }
}
